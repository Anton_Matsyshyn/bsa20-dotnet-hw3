﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingService _parkingService;
        private readonly ILogger<ParkingController> _logger;
        public ParkingController(IParkingService parkingService,
                                 ILogger<ParkingController> logger)
        {
            _parkingService = parkingService;
            _logger = logger;
        }

        [HttpGet("balance")]
        public IActionResult GetBalance()
        {
            try
            {
                return new JsonResult(_parkingService.GetBalance());
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("capacity")]
        public IActionResult GetCapacity()
        {
            try
            {
                return new JsonResult(_parkingService.GetCapacity());
            }
            catch(Exception ex)
            {
                _logger.LogWarning(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("freePlaces")]
        public IActionResult GetFreePlaces()
        {
            try
            {
                return new JsonResult(_parkingService.GetFreePlaces());
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex.Message);
                return BadRequest(ex.Message);
            }
        }
    }
}