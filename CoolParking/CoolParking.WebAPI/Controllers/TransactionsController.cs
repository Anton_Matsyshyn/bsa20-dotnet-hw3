﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CoolParking.BL.Exceptions;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService _parkingService;
        private readonly IMapper _mapper;
        private readonly ILogService _logService;
        private readonly ILogger<TransactionsController> _logger;
        public TransactionsController(IParkingService parkingService,
                                      IMapper mapper,
                                      ILogService logService,
                                      ILogger<TransactionsController> logger)
        {
            _parkingService = parkingService;
            _mapper = mapper;
            _logService = logService;
            _logger = logger;
        }

        [HttpGet("last")]
        public IActionResult GetLastTransaction()
        {
            try
            {
                var transaction = _parkingService
                                  .GetLastParkingTransactions()
                                  .Select(t => _mapper.Map<TransactionInfoModel>(t));

                return new JsonResult(transaction);
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("all")]
        public IActionResult GetAllTransactions()
        {
            try
            {
                return Ok(_logService.Read());
            }
            catch(FileNotFoundException ex)
            {
                _logger.LogWarning(ex.Message);
                return NotFound(ex.Message);
            }
            catch(Exception ex)
            {
                _logger.LogWarning(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("topUpVehicle")]
        public IActionResult TopUpVehicle([FromBody]TopUpModel model)
        {
            try
            {
                _parkingService.TopUpVehicle(model.Id, model.Sum);

                return new JsonResult(_parkingService
                                      .GetVehicles()
                                      .Single(v => v.Id == model.Id));
            }
            catch(VehicleNotExistException ex)
            {
                _logger.LogWarning(ex.Message);
                return NotFound(ex.Message);
            }
            catch(Exception ex)
            {
                _logger.LogWarning(ex.Message);
                return BadRequest(ex.Message);
            }
        }
    }
}