﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CoolParking.BL.Exceptions;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService _parkingService;
        private readonly IMapper _mapper;
        private readonly ILogger<VehiclesController> _logger;
        public VehiclesController(IParkingService parkingService,
                                  IMapper mapper,
                                  ILogger<VehiclesController> logger)
        {
            _parkingService = parkingService;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet()]
        public IActionResult GetVehiclesCollection()
        {
            try
            {
                return new JsonResult(_parkingService
                                      .GetVehicles()
                                      .Select(v => _mapper.Map<VehicleModel>(v)));
            }
            catch(Exception ex)
            {
                _logger.LogWarning(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetVehicle(string id)
        {
            try
            {
                var vehicle = _parkingService.GetVehicle(id);

                return new JsonResult(vehicle);
            }
            catch (VehicleNotExistException ex)
            {
                _logger.LogError(ex.Message);
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [HttpPost()]
        public IActionResult CreateVehicle([FromBody]VehicleModel model)
        {
            try
            {
                Vehicle vehicle = _mapper.Map<Vehicle>(model);
                _parkingService.AddVehicle(vehicle);

                return CreatedAtAction(nameof(GetVehicle), new { id = model.Id }, model);
            }
            catch(Exception ex)
            {
                _logger.LogWarning(ex.Message);
                return BadRequest(ex.Message);
            }
        }
    
        [HttpDelete("{id}")]
        public IActionResult DeleteVehicle(string id)
        {
            try
            {
                _parkingService.RemoveVehicle(id);
                return StatusCode(204);
            }
            catch(VehicleNotExistException ex)
            {
                _logger.LogWarning(ex.Message);
                return NotFound(ex.Message);
            }
            catch(Exception ex)
            {
                _logger.LogWarning(ex.Message);
                return BadRequest(ex.Message);
            }
        }
    }
}