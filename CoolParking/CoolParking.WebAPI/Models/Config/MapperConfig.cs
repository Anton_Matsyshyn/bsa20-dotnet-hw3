﻿using AutoMapper;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Models.Config
{
    public class MapperConfig:Profile
    {
        public MapperConfig()
        {
            CreateMap<Vehicle, VehicleModel>();
            CreateMap<VehicleModel, Vehicle>();

            CreateMap<TransactionInfo, TransactionInfoModel>();
            CreateMap<TransactionInfoModel, TransactionInfo>();
        }
    }
}
