﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking instance;
        private Parking() { }
        public static Parking GetInstance() => instance ??= new Parking();
        public decimal Balance { get; set; }
        public List<Vehicle> Vehicles { get; set; }
        public Queue<TransactionInfo> Transactions { get; set; } = new Queue<TransactionInfo>();
    }
}