﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using CoolParking.BL.Exceptions;
using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public Vehicle(string Id, VehicleType vehicleType, decimal Balance)
        {
            this.Id = CheckIsPlateNumberValid(Id) ? Id : throw new InvalidVehicleIdException(Id);
            VehicleType = (int)vehicleType <= 3 && (int)vehicleType >=0 ? vehicleType : throw new ArgumentException("Invalid vehicle type value (it must be in range of [0-3]).");
            this.Balance = Balance >= 0 ? Balance : throw new ArgumentException("Invalid vehicle balance (it must be non-negative)");
        }
        public string Id { get; set; }
        public VehicleType VehicleType { get; set; }
        public decimal Balance { get; set; }
        public static bool CheckIsPlateNumberValid(string plateNumber)
        {
            try
            {
                Regex regex = new Regex("^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$");
                return regex.IsMatch(plateNumber);
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot validate plate number. See exception below: " + ex.Message);
            }
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            string plateNumber = "AA-1000-AA";

            int indexOfA = 65;
            int indexOfZ = 90;

            try
            {
                Random random = new Random();

                plateNumber = "" + (char)random.Next(indexOfA, indexOfZ + 1)
                              + (char)random.Next(indexOfA, indexOfZ + 1)
                              + '-'
                              + random.Next(1000, 9999)
                              + '-'
                              + (char)random.Next(indexOfA, indexOfZ + 1)
                              + (char)random.Next(indexOfA, indexOfZ + 1);

                return plateNumber;
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot generate plate number. See exception below: "+ex.Message);
            }
        }
    }
}