﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private readonly Timer timer = new Timer();
        public double Interval { get; set; }

        public event ElapsedEventHandler Elapsed;
        public void Dispose() => timer.Dispose();
        public void Start()
        {
            timer.Elapsed += Elapsed;
            timer.Interval = Interval;
            timer.AutoReset = true;
            timer.Enabled = true;
        }

        public void Stop()
        {
            timer.Elapsed -= Elapsed;
            timer.Stop();
        }
    }
}