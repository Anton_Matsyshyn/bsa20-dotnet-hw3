﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Security;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        private readonly ILogger<LogService> _logger;
        public string LogPath { get; }
        public LogService(string logPath) : this(logPath, null) { }
        public LogService(string logPath,
                          ILogger<LogService> logger)
        {
            LogPath = logPath;
            _logger = logger;
        }
        public string Read()
        {
            if (!File.Exists(LogPath))
                throw new FileNotFoundException();

            string file = "";

            try
            {
                file = File.ReadAllText(LogPath);
            }
            catch(Exception ex)
            {
                _logger?.LogWarning(ex.Message);
            }

            return file;
        }

        public void Write(string logInfo)
        {
            try
            {
                if (!File.Exists(LogPath))
                    File.Create(LogPath).Close();

                using StreamWriter sw = new StreamWriter(LogPath, true);
                sw.Write(logInfo+"\n");
            }
            catch (Exception ex)
            {
                _logger?.LogWarning(ex.Message);
            }
        }
    }
}