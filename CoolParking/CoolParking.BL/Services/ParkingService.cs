﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Exceptions;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        readonly ITimerService _withdrawTimer;
        readonly ITimerService _logTimer;
        readonly IConfiguration _config;
        readonly ILogService _logService;
        readonly Parking _parking;
        public ParkingService(ITimerService withdrawTimer,
                              ITimerService logTimer,
                              IConfiguration config,
                              ILogService logService)
        {
            _withdrawTimer = withdrawTimer;
            _withdrawTimer.Elapsed += Withdraw_EventHandler;
            _withdrawTimer.Interval = config.GetValue<double>("Parking:CarPayPeriod")*1000;

            _logTimer = logTimer;
            _logTimer.Elapsed += Logging_EventHandler;
            _logTimer.Interval = config.GetValue<double>("Parking:LoggingPeriod")*1000;

            _config = config;

            _logService = logService;

            _parking = Parking.GetInstance();
            _parking.Balance = config.GetValue<decimal>("Parking:StartBalance");
            _parking.Vehicles = new List<Vehicle>(config.GetValue<int>("Parking:Capacity"));

            _withdrawTimer.Start();
            _logTimer.Start();
        }
        public void AddVehicle(Vehicle vehicle)
        {
            if (_parking.Vehicles.Count == _parking.Vehicles.Capacity)
                throw new InvalidOperationException("There are no free places on the parking.");
            
            if (vehicle == null)
                new InvalidOperationException("Cannot add null vehicle.");

            if (!Vehicle.CheckIsPlateNumberValid(vehicle.Id) || vehicle.Balance < 0)
                throw new ArgumentException("Cannot add vehicle with invalid parameters.");

            if (!CheckIsVehicleIdUnique(vehicle.Id))
                throw new ArgumentException("Cannot add vehicle with non-unique id.");

            _parking.Vehicles.Add(vehicle);
        }

        public void Dispose()
        {
            _withdrawTimer.Dispose();
            _logTimer.Dispose();

            _parking.Balance = 0;
            _parking.Vehicles = new List<Vehicle>(_config.GetValue<int>("Parking:Capacity"));
            _parking.Transactions = new Queue<TransactionInfo>();
        }

        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return _parking.Vehicles.Capacity;
        }

        public int GetFreePlaces()
        {
            return _parking.Vehicles.Capacity - _parking.Vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _parking.Transactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(_parking.Vehicles);
        }
        
        public Vehicle GetVehicle(string id)
        {
            if (!Vehicle.CheckIsPlateNumberValid(id))
                throw new InvalidVehicleIdException(id);

            var vehicle = _parking.Vehicles.FirstOrDefault(v => v.Id == id);

            if (vehicle == null)
                throw new VehicleNotExistException(id);

            return vehicle;
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (!Vehicle.CheckIsPlateNumberValid(vehicleId))
                throw new InvalidVehicleIdException(vehicleId);

            var vehicle = _parking.Vehicles.FirstOrDefault(v => v.Id == vehicleId);

            if (vehicle == null)
                throw new VehicleNotExistException(vehicleId);

            if (vehicle.Balance < 0)
                throw new InvalidOperationException($"Cannot remove vehicle with debt.");

            _parking.Vehicles.Remove(vehicle);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0)
                throw new ArgumentException("Cannot topup negative sum.");

            if (!Vehicle.CheckIsPlateNumberValid(vehicleId))
                throw new InvalidVehicleIdException(vehicleId);

            var vehicle = _parking.Vehicles.FirstOrDefault(v => v.Id == vehicleId);

            if (vehicle == null)
                throw new VehicleNotExistException(vehicleId);

            vehicle.Balance += sum;
        }
        
        public void Withdraw_EventHandler(object sender, ElapsedEventArgs args)
        {
            var tariffsSection = _config.GetSection("Parking:Tariffs");

            foreach(var vehicle in _parking.Vehicles)
            {
                decimal transactionSum = 0;

                switch (vehicle.VehicleType)
                {
                    case VehicleType.PassengerCar:
                        {
                            transactionSum = CalculateTransactionSum(vehicle.Balance, tariffsSection.GetValue<decimal>("PassengerCar"));

                            break;
                        }
                    case VehicleType.Truck:
                        {
                            transactionSum = CalculateTransactionSum(vehicle.Balance, tariffsSection.GetValue<decimal>("Truck"));

                            break;
                        }
                    case VehicleType.Bus:
                        {
                            transactionSum = CalculateTransactionSum(vehicle.Balance, tariffsSection.GetValue<decimal>("Bus"));

                            break;
                        }
                    case VehicleType.Motorcycle:
                        {
                            transactionSum = CalculateTransactionSum(vehicle.Balance, tariffsSection.GetValue<decimal>("Motorcycle"));

                            break;
                        }
                }

                vehicle.Balance -= transactionSum;
                _parking.Balance += transactionSum;

                _parking.Transactions.Enqueue(new TransactionInfo(vehicle.Id, transactionSum));
            }
        }
        public void Logging_EventHandler(object sender, ElapsedEventArgs args)
        {
            string log = "";

            while (_parking.Transactions.Count != 0)
                log += $"{_parking.Transactions.Peek().TransactionDate}: " +
                       $"{_parking.Transactions.Peek().Sum} money withdrawn " +
                       $"from vehicle with Id='{_parking.Transactions.Dequeue().VehicleId}'\n";

            _logService.Write(log);
        }
        private bool CheckIsVehicleIdUnique(string vehicleId)
        {
            return !_parking.Vehicles.Select(v => v.Id).Contains(vehicleId);
        }
        private decimal CalculateTransactionSum(decimal balance, decimal tariff)
        {
            var debtCoeff = _config.GetValue<decimal>("Parking:DebtCoeff"); 
            decimal transactionSum = balance - tariff >= 0 ?
                                     tariff :
                                     balance <= 0 ?
                                     tariff * debtCoeff :
                                     balance + Math.Abs(balance - tariff) * debtCoeff;

            return transactionSum;
        }
    }
}