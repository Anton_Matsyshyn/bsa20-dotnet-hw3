﻿using System;

namespace CoolParking.BL.Exceptions
{
    public class VehicleNotExistException:Exception
    {
        public VehicleNotExistException(string id):base($"Vehicle with id '{id}' does not exist.") { }
    }
}
