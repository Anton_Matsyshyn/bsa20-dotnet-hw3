﻿using System;

namespace CoolParking.BL.Exceptions
{
    public class InvalidVehicleIdException:Exception
    {
        public InvalidVehicleIdException(string id) : base($"Vehicle id '{id}' is invalid.") { }
    }
}
