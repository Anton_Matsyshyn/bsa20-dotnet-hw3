﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.WebClient.Settings
{
    public static class NetSettings
    {
        public static string Host { get; } = "https://localhost:44314/";
        public static string ApiEndpoint { get; } = "api/";

        public static string ParkingEndpoint { get; } = "parking/";
        public static string ParkingEndpoint_Balance { get; } = "balance";
        public static string ParkingEndpoint_Capacity { get; } = "capacity";
        public static string ParkingEndpoint_FreePlaces { get; } = "freePlaces";

        public static string VehicleEndpoint { get; } = "vehicles/";

        public static string TransactionEndpoint { get; } = "transactions/";
        public static string TransactionEndpoint_Last { get; } = "last";
        public static string TransactionEndpoint_All { get; } = "all";
        public static string TransactionEndpoint_TopUpVehicle { get; } = "topUpVehicle";
    }
}
