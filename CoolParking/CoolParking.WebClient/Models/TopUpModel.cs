﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebClient.Models
{
    public class TopUpModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("Sum")]
        public decimal Sum { get; set; }
    }
}
