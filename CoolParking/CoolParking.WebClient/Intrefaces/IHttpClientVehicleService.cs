﻿using CoolParking.WebClient.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoolParking.WebClient.Intrefaces
{
    public interface IHttpClientVehicleService
    {
        Task<IEnumerable<VehicleModel>> GetAllVehicles();
        Task<VehicleModel> GetVehicle(string Id);
        Task<HttpResponseMessage> AddVehicle(VehicleModel model);
        Task<HttpResponseMessage> DeleteVehicle(string vehicleId);
    }
}
