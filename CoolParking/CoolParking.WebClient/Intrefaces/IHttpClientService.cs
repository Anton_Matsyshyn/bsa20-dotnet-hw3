﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.WebClient.Intrefaces
{
    public interface IHttpClientService:IHttpClientParkingService,
                                        IHttpClientVehicleService,
                                        IHttpClientTransactionService
    {
    }
}
