﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.WebClient.Intrefaces
{
    public interface IValidationService
    {
        bool ValidatePlateNumber(string plateNumber);
        bool ValidateBalance(string balance);
        bool ValidateVehicleType(string vehicleType);
    }
}
