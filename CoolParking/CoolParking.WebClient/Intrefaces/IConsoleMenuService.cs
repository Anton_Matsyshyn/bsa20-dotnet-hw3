﻿using System.Threading.Tasks;

namespace CoolParking.WebClient.Intrefaces
{
    public interface IConsoleMenuService
    {
        Task GetVehicle();
        Task AddNewVehicle();
        Task RemoveVehicle();
        Task TopupVehicle();
        Task ParkingBalance();
        Task ParkingTopupFromLastPeriod();
        Task FreePlaces();
        Task ParkingTransactionFromLastPeriod();
        Task AllTransactions();
        Task AllVehicles();
        Task EasterEgg();
    }
}
