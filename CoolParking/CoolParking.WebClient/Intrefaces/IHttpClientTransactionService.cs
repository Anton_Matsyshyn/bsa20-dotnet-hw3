﻿using CoolParking.WebClient.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.WebClient.Intrefaces
{
    public interface IHttpClientTransactionService
    {
        Task<IEnumerable<TransactionInfoModel>> GetLastTransactions();
        Task<string> GetAllTrancsations();
        Task<VehicleModel> TopUpVehicle(TopUpModel model);
    }
}
