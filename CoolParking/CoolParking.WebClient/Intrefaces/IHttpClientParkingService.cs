﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.WebClient.Intrefaces
{
    public interface IHttpClientParkingService
    {
        Task<double> GetParkignBalance();
        Task<int> GetParkingCapacity();
        Task<int> GetParkingFreePlaces();
    }
}
