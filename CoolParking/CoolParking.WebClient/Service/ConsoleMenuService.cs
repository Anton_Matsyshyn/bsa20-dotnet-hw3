﻿using CoolParking.WebClient.Intrefaces;
using CoolParking.WebClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.WebClient.Service
{
    public class ConsoleMenuService : IConsoleMenuService
    {
        private readonly IHttpClientService _clientService;
        private readonly IValidationService _validationService;
        private readonly IMessageService _messageService;
        public ConsoleMenuService(IHttpClientService clientService,
                                  IValidationService validationService,
                                  IMessageService messageService)
        {
            _clientService = clientService;
            _validationService = validationService;
            _messageService = messageService;
        }
        public async Task GetVehicle()
        {
            Console.CursorVisible = true;
            string vehicleId = Validation(id => _validationService.ValidatePlateNumber(id), "Plate number", "Example: FD-5432-YT");

            if (vehicleId == null)
                return;

            VehicleModel vehicle;

            try
            {
                vehicle = await _clientService.GetVehicle(vehicleId);
            }
            catch(Exception ex)
            {
                _messageService.WriteError(ex.Message);
                return;
            }

            Console.WriteLine("Plate num\t\tType\t\tBalance");
            Console.WriteLine();

            Console.WriteLine($"{vehicle.Id}\t\t{vehicle.VehicleType}\t\t{vehicle.Balance}");

            Console.ReadKey();
            Console.CursorVisible = false;
        }
        public async Task AddNewVehicle()
        {
            string vehicleId = "";
            int vehicleBalance = 0;
            int vehicleType;

            Console.CursorVisible = true;

            vehicleId = Validation(id => _validationService.ValidatePlateNumber(id), "Plate number", "Example: FD-5432-YT");

            if (vehicleId == null)
                return;

            var balanceStr = Validation(b => _validationService.ValidateBalance(b), "Vehicle balance", "Vehicle balance must be non-negative");

            if (balanceStr == null)
                return;

            vehicleBalance = int.Parse(balanceStr);

            var vehicleTypeStr = Validation(t => _validationService.ValidateVehicleType(t),
                                            "Vehicle type",
                                            "Vehicle types:\n" +
                                            "0 - Passenger car\n" +
                                            "1 - Truck\n" +
                                            "2 - Bus\n" +
                                            "3 - Motorcycle\n");

            vehicleType = int.Parse(vehicleTypeStr);

            VehicleModel vehicle = null;

            try
            {
                vehicle = new VehicleModel() { Id = vehicleId, VehicleType = vehicleType, Balance = vehicleBalance };
                await _clientService.AddVehicle(vehicle);

                Console.WriteLine("Congratulations! You add new vehicle to parking!");
            }
            catch (Exception ex)
            {
                _messageService.WriteError("Something went wrong :( We cannot create such vehicle.\n" +
                                           ex.Message);
            }

            Console.CursorVisible = false;

            Console.ReadKey();
        }

        public async Task AllTransactions()
        {
            Console.WriteLine(await _clientService.GetAllTrancsations());
            Console.ReadKey();
        }

        public async Task AllVehicles()
        {
            Console.WriteLine("Here is all parking's vehicles");
            Console.WriteLine();

            Console.WriteLine("Plate num\t\tType\t\tBalance");
            Console.WriteLine();

            var vehicles = await _clientService.GetAllVehicles();

            foreach (var vehicle in vehicles)
                Console.WriteLine($"{vehicle.Id}\t\t{vehicle.VehicleType}\t\t{vehicle.Balance}");

            Console.ReadKey();
        }

        public async Task FreePlaces()
        {
            Console.WriteLine($"For now there are {await _clientService.GetParkingFreePlaces()} free places");
            Console.ReadKey();
        }

        public async Task ParkingBalance()
        {
            Console.WriteLine($"For now parking balance is {await _clientService.GetParkignBalance()}");
            Console.ReadKey();
        }

        public async Task ParkingTopupFromLastPeriod()
        {
            var transactions = await _clientService.GetLastTransactions();
            var sum = transactions.Select(t => t.Sum).Sum();


            Console.WriteLine($"Transaction topup from last period: {sum}");

            Console.ReadKey();
        }

        public async Task ParkingTransactionFromLastPeriod()
        {
            var transactions = await _clientService.GetLastTransactions();
            foreach (var t in transactions)
                Console.WriteLine($"Sum: {t.Sum}\n" +
                                  $"Time: {t.TransactionsDate}\n" +
                                  $"Plate number: {t.VehicleId}\n");

            Console.ReadKey();
        }

        public async Task RemoveVehicle()
        {
            Console.CursorVisible = true;

            Console.WriteLine("This section helps you to remove vehicle from parking.");

            var vehicleId = Validation(id => _validationService.ValidatePlateNumber(id),
                                      "vehicle plate number",
                                      "Input id of vehicle, which you want to remove");
            VehicleModel vehicle;

            try
            {
                vehicle = await _clientService.GetVehicle(vehicleId);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }

            Console.WriteLine("Plate num\t\tType\t\tBalance");
            Console.WriteLine($"{vehicle.Id}\t\t{vehicle.VehicleType}\t\t{vehicle.Balance}");
            Console.WriteLine();

            Console.WriteLine("You want to remove this vehicle? (Y/N)");
            var ans = Console.ReadLine().ToLower()[0];

            if (ans != 'y')
                return;

            try
            {
                await _clientService.DeleteVehicle(vehicleId);

                Console.WriteLine();
                _messageService.WriteInformation($"Vehicle with id {vehicleId} was successful removed");
            }
            catch (Exception ex)
            {
                _messageService.WriteError(ex.Message);
            }

            Console.CursorVisible = false;

            Console.ReadKey();
        }

        public async Task TopupVehicle()
        {
            Console.CursorVisible = true;

            Console.WriteLine("This section helps you to topup vehicle.");

            var vehicleId = Validation(id => _validationService.ValidatePlateNumber(id),
                                      "vehicle plate number",
                                      "Input id of vehicle, which you want to topup");

            if (vehicleId == null)
                return;

            VehicleModel vehicle = null;

            try
            {
                vehicle = await _clientService.GetVehicle(vehicleId);
            }
            catch(Exception ex)
            {
                _messageService.WriteError(ex.Message);
            }

            var topupSumStr = Validation(t => int.TryParse(t, out int sum) && sum >= 0,
                                        "topup sum",
                                        "Enter topup sum");

            if (topupSumStr == null)
                return;

            var topupSum = int.Parse(topupSumStr);

            try
            {
                await _clientService.TopUpVehicle(new TopUpModel { Id = vehicle.Id, Sum = topupSum });
                _messageService.WriteInformation($"Vehicle {vehicleId} was successful topuped");
            }
            catch (Exception ex)
            {
                _messageService.WriteError(ex.Message);
            }

            Console.CursorVisible = false;

            Console.ReadKey();
        }
        public async Task EasterEgg()
        {
            Console.WriteLine("О боже, дякую що ви зайшли сюди!");
            Console.WriteLine();
            Console.ReadKey();

            Console.WriteLine("Розробники викрали мене i заховали у цiй програмi!..");
            Console.WriteLine();
            Console.ReadKey();

            Console.WriteLine("Для чого? А самi не розумiєте?");
            Console.WriteLine();
            Console.ReadKey();

            Console.WriteLine("Хто ж iще буде виставляти цi бiли лiтери на чорному фонi?");
            Console.WriteLine();
            Console.ReadKey();

            Console.WriteLine("Що-що? Якi команди, якi байти i бiти, це все роблю я.");
            Console.WriteLine();
            Console.ReadKey();

            Console.WriteLine("Дiдько! Сюди прямує збiрщик смiття! Менi граблi...");
            Console.WriteLine();
            Console.ReadKey();

            Console.WriteLine("I пам'ятайте: реальнiсть - iлюзiя");
            Console.WriteLine();
            Console.ReadKey();

            Console.WriteLine("Усесвiт - голограма");
            Console.WriteLine();
            Console.ReadKey();

            Console.WriteLine("Скупайте золото");
            Console.WriteLine();
            Console.ReadKey();

            Console.WriteLine("Па-па");
            Console.WriteLine();
            Console.ReadKey();

        }
        private string Validation(Func<string, bool> validation, string paramName, string description)
        {
            do
            {
                Console.WriteLine("Input correct " + paramName);
                Console.WriteLine(description);

                var str = Console.ReadLine();

                if (validation(str))
                {
                    Console.WriteLine();
                    _messageService.WriteInformation(paramName + " is correct.");
                    Console.WriteLine();

                    return str;
                }
                else
                {
                    Console.WriteLine();
                    _messageService.WriteError(paramName + " is not valid. Try one more time? (Y/N)");
                    Console.WriteLine();

                    if (Console.ReadLine().ToLower()[0] != 'y')
                    {
                        Console.WriteLine("Tap any key to go to the menu.");
                        Console.ReadKey();

                        return null;
                    }
                }

            } while (true);
        }
    }
}
