﻿using CoolParking.WebClient.Intrefaces;
using CoolParking.WebClient.Models;
using CoolParking.WebClient.Settings;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.WebClient.Service
{
    public class HttpClientService:IHttpClientService
    {
        private readonly HttpClient _client;
        public HttpClientService()
        {
            _client = new HttpClient { BaseAddress = new Uri(NetSettings.Host + NetSettings.ApiEndpoint) };
        }
        //IHttpClientParkingService methods
        public async Task<double> GetParkignBalance()
        {
            string requestUrl = NetSettings.ParkingEndpoint + NetSettings.ParkingEndpoint_Balance;

            var response = await _client.GetAsync(requestUrl);

            await CheckResponse(response);

            var content = response.Content;

            try
            {
                double balance = JsonConvert.DeserializeObject<double>(await content.ReadAsStringAsync());
                return balance;
            }
            catch(Exception ex)
            {
                Console.WriteLine("Something went wrong. See exception here:");
                Console.WriteLine(ex.Message);
            }

            return 0;
        }
        public async Task<int> GetParkingCapacity()
        {
            string requestUrl = NetSettings.ParkingEndpoint + NetSettings.ParkingEndpoint_Capacity;

            var response = await _client.GetAsync(requestUrl);

            await CheckResponse(response);

            var content = response.Content;

            try
            {
                int capacity = JsonConvert.DeserializeObject<int>(await content.ReadAsStringAsync());
                return capacity;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong. See exception here:");
                Console.WriteLine(ex.Message);
            }

            return 0;
        }
        public async Task<int> GetParkingFreePlaces()
        {
            string requestUrl = NetSettings.ParkingEndpoint + NetSettings.ParkingEndpoint_FreePlaces;

            var response = await _client.GetAsync(requestUrl);

            await CheckResponse(response);

            var content = response.Content;

            try
            {
                int freePlaces = JsonConvert.DeserializeObject<int>(await content.ReadAsStringAsync());
                return freePlaces;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong. See exception here:");
                Console.WriteLine(ex.Message);
            }

            return 0;
        }

        //IHttpClientVehicleService methods
        public async Task<IEnumerable<VehicleModel>> GetAllVehicles()
        {
            string requestUrl = NetSettings.VehicleEndpoint;

            var response = await _client.GetAsync(requestUrl);

            await CheckResponse(response);

            var content = response.Content;

            try
            {
                var vehicles = JsonConvert.DeserializeObject<IEnumerable<VehicleModel>>(await content.ReadAsStringAsync());
                return vehicles;
            }
            catch(Exception ex)
            {
                Console.WriteLine("Something went wrong. See exception here:");
                Console.WriteLine(ex.Message);
            }

            return null;
        }
        public async Task<VehicleModel> GetVehicle(string vehicleId)
        {
            string requestUrl = NetSettings.VehicleEndpoint + vehicleId;

            var response = await _client.GetAsync(requestUrl);

            await CheckResponse(response);

            var content = response.Content;

            try
            {
                VehicleModel vehicle = JsonConvert.DeserializeObject<VehicleModel>(await content.ReadAsStringAsync());
                return vehicle;
            }
            catch(Exception ex)
            {
                Console.WriteLine("Something went wrong. See exception here:");
                Console.WriteLine(ex.Message);
            }

            return null;
        }
        public async Task<HttpResponseMessage> AddVehicle(VehicleModel model)
        {
            string requestUrl = NetSettings.VehicleEndpoint;

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            var response = await _client.PostAsync(requestUrl,content);
            await CheckResponse(response);

            return response;
        }
        public async Task<HttpResponseMessage> DeleteVehicle(string vehicleId)
        {
            string requestUrl = NetSettings.VehicleEndpoint + vehicleId;

            var response = await _client.DeleteAsync(requestUrl);
            await CheckResponse(response);
            
            return response;
        }

        //IHttpClientVehicleService methods
        public async Task<IEnumerable<TransactionInfoModel>> GetLastTransactions()
        {
            string requestUrl = NetSettings.TransactionEndpoint + NetSettings.TransactionEndpoint_Last;

            var response = await _client.GetAsync(requestUrl);
            await CheckResponse(response);

            var content = response.Content;

            try
            {
                TransactionInfoModel[] transactions = JsonConvert.DeserializeObject<TransactionInfoModel[]>(await content.ReadAsStringAsync());
                return transactions;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong. See exception here:");
                Console.WriteLine(ex.Message);
            }

            return null;
        }
        public async Task<string> GetAllTrancsations()
        {
            string requestUrl = NetSettings.TransactionEndpoint + NetSettings.TransactionEndpoint_All;

            var response = await _client.GetAsync(requestUrl);
            await CheckResponse(response);

            var content = response.Content;

            try
            {
                string transactions = await content.ReadAsStringAsync();
                return transactions;
            }
            catch(Exception ex)
            {
                Console.WriteLine("Something went wrong. See exception here:");
                Console.WriteLine(ex.Message);
            }

            return null;
        }
        public async Task<VehicleModel> TopUpVehicle(TopUpModel model)
        {
            string requestUrl = NetSettings.TransactionEndpoint + NetSettings.TransactionEndpoint_TopUpVehicle;
            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            var response = await _client.PutAsync(requestUrl, content);
            await CheckResponse(response);

            var responseContent = response.Content;

            try
            {
                VehicleModel vehicle = JsonConvert.DeserializeObject<VehicleModel>(await responseContent.ReadAsStringAsync());
                return vehicle;
            }
            catch(Exception ex)
            {
                Console.WriteLine("Something went wrong. See exception here:");
                Console.WriteLine(ex.Message);
            }

            return null;
        }
    
        private async Task CheckResponse(HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode)
                return;

            var message = await response.Content.ReadAsStringAsync();

            throw new Exception(response.StatusCode+":\t"+message);
        }
    }
}
