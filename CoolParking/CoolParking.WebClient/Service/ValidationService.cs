﻿using CoolParking.WebClient.Intrefaces;
using System;
using System.Text.RegularExpressions;

namespace CoolParking.WebClient.Service
{
    public class ValidationService : IValidationService
    {
        public bool ValidateBalance(string balance) => int.TryParse(balance, out int balanceInt) && balanceInt >= 0;

        public bool ValidatePlateNumber(string plateNumber)
        {
            try
            {
                Regex regex = new Regex("^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$");
                return regex.IsMatch(plateNumber);
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot validate plate number. See exception below: " + ex.Message);
            }
        }

        public bool ValidateVehicleType(string vehicleType) => int.TryParse(vehicleType, out int vehicleTypeInt) && vehicleTypeInt >= 0 && vehicleTypeInt <= 3;
    }
}
