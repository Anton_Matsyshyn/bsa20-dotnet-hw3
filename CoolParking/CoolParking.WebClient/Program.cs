﻿using CoolParking.WebClient.ConsoleMenu;
using CoolParking.WebClient.Service;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoolParking.WebClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Menu menu = new Menu(new List<Item>
                                 {
                                    new Item("Vehicles",new List<Item>{
                                        new Item("Get vehicle"),
                                        new Item("Add new vehicle") ,
                                        new Item("Remove vehicle") ,
                                        new Item("Topup vehicle"),
                                        new Item("             ")
                                    }),
                                    new Item("Parking",new List<Item>{
                                        new Item("Parking balance"),
                                        new Item("Parking topup from last period"),
                                        new Item("Free places"),
                                        new Item("Parking transactions from last period"),
                                        new Item("All transactions"),
                                        new Item("All vehicles")
                                    })
                                 }, new ConsoleMenuService(new HttpClientService(),
                                                           new ValidationService(),
                                                           new MessageService()));
            await menu.Start();

            Console.WriteLine("Hello World!");
        }
        static async Task t()
        {
            HttpClientService service = new HttpClientService();

            var b = await service.GetParkignBalance();
            var c = await service.GetParkingCapacity();
            var d = await service.GetParkingFreePlaces();
            var e = await service.GetAllVehicles();
            var g = await service.AddVehicle(new Models.VehicleModel { Id = "HG-6789-OP", Balance = 100.9M, VehicleType = 2 });

            var f = await service.GetVehicle("HG-6789-OP");
            var h = await service.DeleteVehicle("HG-6789-OP");
        }
    }
}
