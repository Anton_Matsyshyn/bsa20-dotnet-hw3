﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.WebClient.ConsoleMenu
{
    public class Item
    {
        public string Title { get; set; }
        public string Name { get; set; }
        public List<Item> ChildItems { get; set; }
        public Item(string name) : this(name, null) { }
        public Item(string name, List<Item> childItems)
        {
            Name = name;
            ChildItems = childItems;
        }
        public override string ToString()
        {
            return Name;
        }
    }
}
